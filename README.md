# Rogue-like

## Manuel Utilisateur

Le rogue-like est un genre de jeux vidéo dont le gameplay est inspiré de Rogue, sorti sur Berkeley Unix en 1980. 
Traditionnellement, le joueur y explore au tour par tour un ou plusieurs souterrains générés aléatoirement, 
dans un univers bi-dimensionnel décrit par des caractères ASCII.
Dans version du rogue-like le joueur se promène dans un dongeon truffé de monstres qui essaient de le tuer,
notre joueur peux également ramasser des équipements à savoir des armes, des potions magiques, et il peut les 
utiliser sur les monstres. Dans notre scénario, le joueur doit parcourir tout le dongeon à la recherche 

### Lancement du jeu

Récupérer le fichier .jar du projet et l'exécuter en tappant la commande suivant:
java -jar roguelike-1.0-SNAPSHOT-jar-with-dependencies.jar

### Utilisation
Touches:  
* w : Avancer  
* s : Reculer  
* a : Aller à gauche  
* d :Aller à droite  
* space : Ramasser un équipement  
* e : Tirer sur un monstre avec une arme  
entrer : pour valider un choix  

### Personnages et équipement du jeu
*| : Munitions  
*/ : Arme  
*. : Or  
*8 : Potion  
Z: Monstres (zombies)  


## Manuel technique

### Outils utilisés

Pour parvenir à la réalisation du jeu on a utilisé le langage de développement JAVA, 
dans sa version 8 et comme outil de gestion et d'automatisation de production nous avons utilisé Maven.
Comme système de suivi des versions git/bitbucket ont été utilisés. Le framework de test unitaire JUnit 
dans sa version 4 a été utilisé.

### Conception du jeu

Nous avons representer les principales classes de notre conception dans la figure suivante.

![GitHub Logo](/home/ouedraogo/Téléchargements/RogueLikePic.png)

Nous avons une classe **Player** qui hérite de la classe **Entity**, on a aussi **Creature** qui hérite de **Entity**
Un **Player** se trouve dans un monde matérialisé par la classe **World**.Le monde contient des niveaux appelés **Level**, 
qui sont générés par **LevelGenerator**. La classe Level est responsable du niveau de difficulté de jeu, à un certain moment 
la difficulté augmente au fur à mesure que le joueur grimpe de niveau en niveau.
Un **Player** possède un **Inventaire** (On affice les inventaires grâce aux **Screen**) d'**Item**, ici les items sont les 
munitions (**Ammo**), les armures (**Armor**), l'Or (**Gold**), les pistolets (**Gun**), Les potions de magie (**HealthPotion**),
les potions (**Potion**), les épées (**Sword**) et les armes (**Weapon**).Le **Player** est attaqué par les **créatures** pour 
l'empêcher de retrouver l'objet qu'il recherche. La classe PathFIndingAI permet à une **Entity** de trouver le chemin à une 
autre **entity**.





