package roguelike;

import static org.junit.Assert.*;

import java.awt.Color;

import org.junit.Before;
import org.junit.Test;

import items.Item;

public class CreatureTest {
	
	Creature mycreature;
	public Entity[][] Map;
	
	@Before
	public void init() {
		mycreature = new Creature("Rex", 500, 1, '^', 15, 26, Color.blue);
		//Level mylevel = new Level("NIveau 2",Map,);
	}
	
	@Test
	public void testTakeDamage() {
		int healthly = mycreature.health;
		mycreature.takeDamage(180);
		assertTrue(mycreature.health < healthly);
	}

	@Test
	public void testSetHealth() {
		int healthly = mycreature.health;
		mycreature.setHealth(9000);
		assertTrue(healthly < mycreature.health );
	}

	@Test
	public void testCheckPlayerKill() {
		//mycreature.setLevel(5);
		mycreature.setHealth(0);
		//mycreature.checkPlayerKill();
	}

	@Test
	public void testAttackPlayer() {
	}

	@Test
	public void testDie() {
	}

}
