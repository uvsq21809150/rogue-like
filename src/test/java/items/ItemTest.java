package items;

import static org.junit.Assert.*;

import java.awt.Color;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ItemTest {
	Item monitem;
	
	@Before
	public void init() {
		monitem = new Item("Potion magique",'£',5,5,Color.green);
	}
	
	@Test
	public void testGetName() {
		assertEquals("Potion magique", monitem.getName() );
	}

	@Test
	public void testGetItemID() {
		assertNotNull( monitem.getID());
	}

	@Test
	public void testSetItemID() {
		monitem.setID('1');
		assertEquals('1', monitem.getID());
	}
	
	@After
	    public void destroy() {
		monitem = null;
	    }


}
